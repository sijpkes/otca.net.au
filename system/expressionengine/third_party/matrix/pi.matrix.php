<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*The MIT License (MIT)
*
*Copyright (c) 2013 Paul Sijpkes.
*
*Permission is hereby granted, free of charge, to any person obtaining a copy
*of this software and associated documentation files (the "Software"), to deal
*in the Software without restriction, including without limitation the rights
*to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*copies of the Software, and to permit persons to whom the Software is
*furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in
*all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*THE SOFTWARE.
**/

if(!method_exists('JSMIN', 'minify')) {
	require_once("libs/JSMin.php");
}
if(!defined('MINIFY')) {
	define('MINIFY', TRUE);
}

$plugin_info = array(
	'pi_name' => 'Criteria Matrix',
	'pi_version' => '1.9.1',
	'pi_author' => 'Paul Sijpkes',
	'pi_author_url' => '',
	'pi_description' => 'Criteria Matrix app for student self-assessment',
	'pi_usage' => Matrix::usage()
);

class Matrix {
 
    public $return_data = "";
    private $id = 0;
    private $member_id = 0;
    
    public function __construct()
    {
    	ee()->lang->loadfile('matrix');
        $this->id = ee()->TMPL->fetch_param('id');
        $this->member_id = ee()->session->userdata('member_id');
    }
   
 /**
 * StudentJavascript
 * 
 * 
 * 
 */
public function studentJavascript() {
$emptyUserProfile = "";	
$form = "";
$cycle_result = "";

if($this->id === "guest")  {
	$emptyUserProfile = "window.userProfile = {
			\"reflections\" : {},
			\"startNewCycle\" : false,
			\"steps\" : [],
			\"level\" : 0,
			\"beginner\" : true,
			\"objectives\" : [],
			\"history_id\" : 0,
			\"title\" : \"\",
			\"time\" : 0
	}; 
	
	window.stepDefinitions = [ \"null\", \"Request for Service\", \"Information Gathering\", \"Occupational Assessment\", \"Identification of Occupational Issues\", \"Goal Setting\", \"Intervention\",
\"Evaluation\", \"Being a Professional\" ];"; 
}

$form .= self::embedProgressPlugin();
$form .= self::fetchStudentAppJS($this->id, $emptyUserProfile, $cycle_result, ee()->TMPL->tagdata);
return $form;
}

private static function embedProgressPlugin() {
     ob_start();
    include 'jquery.matrix-progress.js';
    $str = ob_get_clean();
    $js = MINIFY ? JSMin::minify($str) : $str;
    return "<script>$js</script>";
}

private static function fetchStudentAppJS($id, $emptyUserProfile = "", $cycle_result, $info) {
    $id = is_string($id)?"\"$id\"":$id;
    
    $selector = ee()->TMPL->fetch_param('jquery-selector');
    $legend_title = ee()->TMPL->fetch_param('legend-title');
    //$current = ee()->TMPL->fetch_param('legend-current');
    $previous =  ee()->TMPL->fetch_param('legend-previous');
    $waiting =  ee()->TMPL->fetch_param('legend-waiting');
    $colors = explode(",", ee()->TMPL->fetch_param('legend-colors'), 3);
    $contrast = explode(",", ee()->TMPL->fetch_param('text-contrast-colors'), 3);
    $verified = ee()->TMPL->fetch_param('verified-competency-statement-link');
    $unverified = ee()->TMPL->fetch_param('unverified-competency-statement-link');
    $competency_link_title = ee()->TMPL->fetch_param('competency-link-title');
    $step_label = ee()->TMPL->fetch_param('step-label');
    
    foreach($colors as $key => $color) {
	$colors[$key] = trim($color);
    }
    
    $info = json_encode($info); 
	ee()->lang->loadfile('matrix');
	
    ob_start();
	include 'studentEvidencingApp.js';
    $str = ob_get_clean();
    $js = MINIFY ? JSMin::minify($str) : $str;

return "<script>$js</script>";
}
   
public static function usage()
{
    ob_start();  ?>

This is the OTCEM plugin for the student view.

    <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }
}
