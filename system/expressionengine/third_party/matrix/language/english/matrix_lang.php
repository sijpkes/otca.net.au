<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang = array(	
	'not_yet_assessed' => 'You have not yet self-assessed.',
	'guests_may_not_assess' => 'Guests may not self-assess.  Contact your subscribing institution if you wish to create a login.',
	'upload_form_info' => "When you click 'Save', this OTCEM Self assessment and any uploaded supporting items will be saved as evidence fulfilling: ",
	'upload_form_steps' => "Step(s)",
	'upload_form_latest_ppc' => "in your <strong style='font-size:14px'>latest PPC</strong> which is", 
	'upload_success_main_title' => "Upload Successful",
	'upload_success_message_title' => "Self-assessment / evidence Saved.",
	'upload_success_message_sub' => "You can view this in <em>'Your Self-Assessment Progress'</em> list.",
	'upload_success_message_von' => "This evidence will not count towards your competencies until your practice educator has validated your self-assessment",
	'otcem_ticked_on' => 'self-assessed on',
	'otcem_alt_text_glasses' => 'Eye glasses, roll mouse over this box to look',
	'otcem_button_okay' => 'O.K.',
	'otcem_self_assessment_msg' => 'You are now self-assessing.<br>Steps self-assessed so far...',
	'otcem_save_upload_button' => 'Save and Upload',
	'otcem_close_button' => 'Close',
	'otcem_refresh_button' => 'Refresh'
);

/* End of file lang.ee_learning_tools_integration.php */
/* Location: /system/expressionengine/third_party/ee_learning_tools_integration/language/english/lang.ee_learning_tools_integration.php */
