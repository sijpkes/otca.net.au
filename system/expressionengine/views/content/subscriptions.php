<?php extend_template('default') ?>
	
	<?php if($expired):  ?>
		<p style='color: red'><strong><?= $institution_name ?> does not currently have an active subscription</strong>.  Your subscription expired on <strong><?= $expiry_date ?></strong>.
			New students will not be able to register and existing users from <?=$institution_name?> will not have access to the subscribed areas of the site.</p>
			
	<?php endif; ?>	
	<?php if(!$expired): ?>
	<p>Your subscription expires on: <b><?= $expiry_date ?></b></p>
		<ul class="bullets">
			
			<li>
				<h3>Student registration link</h3>
				<p>
				Place this link on the course page for students using <?=$institution_name?>'s LMS.
				<textarea>
					<?=$student_url?>
				</textarea>
				</p>
			</li>
			
			<li>
				<h3>Educator registration link</h3>
				<p>
				Paste this link into an email to the practice educators that will observe students.
				<textarea>
					<?=$educator_url?>
				</textarea>
			</p></li>
			
			<li>
				<h3>Lecturer registration link</h3>
				<p>
				Email this link to your Lecturers.
				<textarea>
					<?=$lecturer_url?>
				</textarea>
				</p>
			</li>
			<li>
				<h3>Use Practice Educator Verification?</h3>
				<p>
					<ul>
				<li>If your students are getting their self-assessments verified by a Practice Educator via the OTCA website, then turn this on.</li>
				<lI>If you just want to provide the self-assessment tool without Practice Educator verification, turn this feature off.</li>
				</ul>
			<?php 
				echo form_open('C=subscription_details'.AMP.'M=institutions_use_verification');
			  	echo form_hidden('action', 'use_verification');
			?>
				<span>
					Practice Educator Verification is:
					<label style='display: inline; margin: 1em;'>On 
					<input type="radio" name="use_verification" <?= $use_verification == 1 ? "checked='1'" : "" ?> value="on"/> </label>
					<label style='display: inline; margin: 1em;'>Off
					<input type="radio" name="use_verification" <?= $use_verification == 0 ? "checked='1'" : "" ?> value="off"/> </label>
				</span>
			<?php
				echo form_submit('effect_institutions', $use_verification_button_label, 'class="submit"');
			  	echo form_close(); 
			?>
		</p>
			</li>
		</ul>
	<?php endif; ?>