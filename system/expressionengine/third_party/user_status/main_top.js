	window.guest = <?= $is_guest ? 'true' : 'false' ?>;
	/*
	 *  Some database JSON data is returned as quote enclosed, others not, this checks.
	 * 
	 */
	window.safeParseObject = function(object) {
		var return_data;
		try {
    		if(typeof object == 'string') {
    			return_data = JSON.parse(object);
    		} else if (typeof object == 'array' || typeof object == 'object') {
    			return_data = object;
    		}
    		} catch(e) {
    			if(typeof return_data == 'undefined') {
    				return_data = "no data defined";
    			}
    			console.log("JSON data corrupted. >>> "+return_data.toString()+ " >>>> "+object.toString());
    			return_data = {};
    		}
    		
   	 return return_data;
	};
	
	$.checkHistoryID = function(callback, noCallbackIfUserLoggedIn) {
	if(typeof noCallbackIfUserLoggedIn == 'undefined') noCallbackIfUserLoggedIn = false;
		
	if(typeof window.userProfile !== 'undefined' && !window.guest) {
		if(window.userProfile.history_id == 0) {
			$('div#dialog-box > span#content').append("<h2>Start a new Practice Placement Cycle (PPC)</h2>\
								  <p>You start a new PPC on this website whenever you start a new placement.</p>\
								  <p>You are now starting a new practice placement cycle, you will need to name it before continuing.\
								  This will save it to your profile for future use.</p><p>You can restore previous cycles from the toolbar\
								  at any time by clicking\
								  'Manage Practice Placement Cycles' and selecting the relevant cycle.</p><p>We recommend that you save\
								  your current cycle with a meaningful name.\
								  <b>Note that any self-assessment done via the OTCEM is always placed in the <span style='font-size: 14pt'>latest</span> PPC</b>\
								  </p><label for='cycle_name'><strong>Cycle name:</strong></label>\
								  <input name='cycle_name' type='text' />\
								  <button name='save'>Save</button>");
			$('div#dialog-box').show(100);
		
		var process = function(e) {
			
				// hack for IE 9 bug where this is added twice					
				if($('#dialog-box').length > 1) {
					$('#dialog-box').last().remove();
					console.log("removed duplicate file-viewer DIV");
				}
				
				var cycle_name = $("#dialog-box input[name='cycle_name']").val();
				
				if(cycle_name !== null && typeof cycle_name !== 'undefined' && cycle_name.length > 0) {
						window.userProfile.title = cycle_name;
						
						$('#ajax-loader').show();
						$.post('/ajax/user-status', 
							{ "userProfile" : JSON.stringify(window.userProfile) }, 
							function(data) {
								if(typeof data.error === 'undefined') {
									$('#ajax-loader').hide();
									window.userProfile.history_id = data.history_id;
									callback(cycle_name);
								} else {
										$("#dialog-box #errorMsg").remove();
										$("#dialog-box input[name='cycle_name']").css('border', 'thin solid red').after("<p style='color: red' id='errorMsg'>There was a problem saving this cycle, please try again.</p>");
								}
							} 
							,'json');
				} else {
					$("#dialog-box #errorMsg").remove();
					$("#dialog-box input[name='cycle_name']").css('border', 'thin solid red').after("<p style='color: red' id='errorMsg'>Please enter a name for this cycle</p>");
					return false;
				}
		return false;
		};
		
		$(document).on('keypress', "#dialog-box input[name='cycle_name']", function(e) {
			if(e.which == 13) {
				process(e);
			}
		});
		
		$(document).on('click', "#dialog-box button[name='save']", function(e) {
				e.preventDefault();
				process(e);
		});
		
		} else {
			if(!noCallbackIfUserLoggedIn) {
				callback();
			}
		}
	} 
	return this;
	};
	
	var searchGoogle = function() {
		var search_val = $("#search input#keywords").val();
		search_val = encodeURIComponent(search_val);
		window.document.location = "//www.google.com/#hl=en&sclient=psy-ab&q="+search_val+"+site:otca.net.au";
	};
	
//search box
	$("#search input[type='submit']").click(function(e) {
				e.preventDefault();
				searchGoogle();
	});
	
	$("#search input#keywords").keypress(function(event) {
		if ( event.which == 13 ) {
			event.preventDefault();
			searchGoogle();
		}
	});
