<?php /**
*The MIT License (MIT)
*
*Copyright (c) 2013 Paul Sijpkes.
*
*Permission is hereby granted, free of charge, to any person obtaining a copy
*of this software and associated documentation files (the "Software"), to deal
*in the Software without restriction, including without limitation the rights
*to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*copies of the Software, and to permit persons to whom the Software is
*furnished to do so, subject to the following conditions:
*
*The above copyright notice and this permission notice shall be included in
*all copies or substantial portions of the Software.
*
*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
*THE SOFTWARE.
*/ ?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
POST a JSON object to this script to persist the current practice cycle.
The profile consists of user reflections and general settings for the practice cycle.  
Reflections (javascript: userProfile.reflections[]) are persisted in the otca_diary mySQL table.
The user status (javascript: userProfile.steps, userProfile.level, userProfile.beginner, userProfile.objectives) 
are persisted to the otca_user_status mySQL table.
*/

$plugin_info = array(
    'pi_name' => 'OTCA User Status',
    'pi_version' => '1.0.0',
    'pi_author' => 'Paul Sijpkes',
    'pi_author_url' => '',
    'pi_description' => 'User status plugin',
    'pi_usage' => User_status::usage()
);

class User_status {

private $USE_VERIFICATION = TRUE; // use practice educator verification for this user?

private $MAX_ASSESSED_ITEMS = 100000; // max number of items to view in history, for pagination purposes --- PAGINATION IS BROKEN!! See Issue #5 
private $START_ASSESS = 0;

private $member_id = 0;
public $return_data = "";
    
public function __construct() {
	$this->member_id = ee()->session->userdata('member_id');
	if(empty($this->member_id)) return FALSE;
	
	$this->USE_VERIFICATION = $this->use_verification_check();
	$this->START_ASSESS = 0;
	
	if(ee()->session->flashdata('start_page') === FALSE) {
		ee()->db->select("assessment");
		$query = ee()->db->get("otca_pagination");
		$res = $query->row();
		
		if(!empty($res)) {
			$this->START_ASSESS = $res->assessment;
			ee()->session->set_flashdata('start_page', $res->assessment);
		}
	} else {
		$this->START_ASSESS = ee()->session->flashdata('start_page');
	}
}

private function use_verification_check() {
	$query = "SELECT use_verification FROM otca_institutions WHERE id 
			  = (SELECT institution_id FROM exp_otca_member_fields WHERE member_id = '$this->member_id')";	
	
	$res = ee()->db->query($query);
	$row = $res->row();
	//echo "<div style='display:none'>$query = USE VERIFY >>>> $row->use_verification</div>";
	return $row->use_verification == 1 ? TRUE : FALSE;
}

private function assess_count() {
	//$query = ee()->db->query("SELECT COUNT(*) as count FROM `otca_evidence` LEFT JOIN `exp_channel_titles` ON `otca_evidence`.`entry_id` = `exp_channel_titles`.`entry_id` WHERE `author_id` = '$this->member_id'");
	
	$query = ee()->db->query("SELECT COUNT(*) count
					FROM exp_channel_data data 
					LEFT JOIN exp_channel_titles title ON title.entry_id = data.entry_id 
					LEFT JOIN otca_evidence ev ON title.entry_id = ev.entry_id 
					WHERE title.author_id = '$this->member_id'");
	
	$res = $query->row();

	return $res->count;
}

private function update_page($start) {
		$sql = "INSERT INTO exp_otca_pagination (member_id, assessment) VALUES ('$this->member_id','$start')
				ON DUPLICATE KEY UPDATE assessment = '$start'";
				
		ee()->db->query($sql);
		
		ee()->session->set_flashdata('start_page', $start);
		ee()->functions->redirect(ee()->config->item('site_url').'/'.ee()->uri->uri_string());
}

private function next_page() {
	$row_count = $this->assess_count();
	$start = $this->START_ASSESS + $this->MAX_ASSESSED_ITEMS;
	
	$this->update_page($start);
}

private function prev_page() {
	$start = $this->START_ASSESS - $this->MAX_ASSESSED_ITEMS;
	if($start < 0) $start = 0;
	$this->update_page($start);
}

private function has_next_assess() {
	$start = ee()->session->flashdata('start_page');
	//echo "start: ".$start . " end: " . $this->assess_count();
	return ($start + $this->MAX_ASSESSED_ITEMS) < $this->assess_count();
}

private function has_prev_assess() {
	$start = ee()->session->flashdata('start_page');
	return ($start > 0);
}

private function max_assessments_per_page() {
	return $this->MAX_ASSESSED_ITEMS;
}

public function save() {
    if(!isset($this->member_id) || $this->member_id == 0) return;

$userProfileInput = ee()->input->post('userProfile');

if(!empty($userProfileInput)) {
    $userProfile = json_decode($userProfileInput);
    
    /* fail safe for empty PRACSOTS */
    foreach($userProfile->objectives as &$obj) {
        if(empty($obj->pracsot)) {
            $error_msg = array("error" => "Error saving user profile! PRACSOT not set for $obj->text");
            return json_encode($error_msg);
        }
    }
    
    
    if($userProfile->startNewCycle == 'true') 
        $endCycle = TRUE;
    else 
        $endCycle = FALSE;
    
        $steps = isset($userProfile->steps)?mysql_real_escape_string(json_encode($userProfile->steps)):'';
        $level = isset($userProfile->level)?$userProfile->level:0;
        $beginner = isset($userProfile->beginner)?$userProfile->beginner:0;
        $objectives = isset($userProfile->objectives)?mysql_real_escape_string(json_encode($userProfile->objectives)):"[]";
        $title = isset($userProfile->title)?mysql_real_escape_string($userProfile->title):"";
    
    if(empty($title)) {
        $error_msg = array("error" => "Error saving user profile!");
        return json_encode($error_msg);
    }
    
    // create blank history item ready for saving this in the history later on... and so we can get the history_id  
    if($userProfile->history_id == 0) {
            $query = ee()->db->query("INSERT INTO `otca_user_status_history` (`member_id`, `steps`, `level`, `beginner`,`objectives`, `title`, `time`) VALUES ('$this->member_id','$steps','$level','$beginner','$objectives', '$title', UNIX_TIMESTAMP() );");
            $userProfile->history_id = ee()->db->insert_id(); 
     }
    $history_id = $userProfile->history_id;
    foreach($userProfile->reflections as &$entry) {
       $text = mysql_real_escape_string($entry->text);
          
       $timestamp = $entry->date;   
       $entry_id = isset($entry->internalId)?$entry->internalId:0;  
       $tag = isset($entry->tag)?$entry->tag:'';
            
          // echo "$text >>>> $tag \n\n"; 
            
          if(!empty($text) && !empty($tag)) {
        if($entry_id != 0) {
            $query =  ee()->db->query("UPDATE `otca_diary` SET `entry_text`='$text', `last_updated`='$timestamp' WHERE `member_id`='$this->member_id' AND `entry_id`='$entry_id' AND `tag` = '$tag'"); 
        } else {        
            $query =  ee()->db->query("INSERT INTO `otca_diary` (`member_id`,`entry_text`,`creation_date`, `current_practice_cycle`, `tag`) VALUES ('$this->member_id','$text','$timestamp','$history_id', '$tag')");
        }
           }
    }
// save status
ee()->db->query("INSERT INTO `otca_user_status` (`member_id`, `steps`, `level`, `beginner`,`objectives`,`title`, `history_id`)
                            VALUES ('$this->member_id','$steps','$level','$beginner','$objectives', '$title', '$history_id')
                            ON DUPLICATE KEY UPDATE `steps`='$steps', `level`='$level', `beginner`='$beginner', `objectives`='$objectives',
                            `title`='$title', `history_id`='$history_id'");

// update history
ee()->db->query("UPDATE otca_user_status_history a, otca_user_status b SET a.steps = b.steps, a.level = b.level, a.beginner = b.beginner,a.objectives = b.objectives, a.title = b.title WHERE a.id = b.history_id AND a.member_id = '$this->member_id'");

return json_encode($userProfile->history_id);
}
}

public function restore() {
if(empty($this->member_id)) return;	
$next = ee()->input->get('nextassess');
$prev = ee()->input->get('prevassess');

if(!empty($next)) {
	$this->next_page();
	return;
}
if(!empty($prev)) {
	$this->prev_page();
	return;
}

$member_id = $this->member_id;
$group_id = ee()->session->userdata('group_id');

$admin_toolbars = "";
if ($group_id == 1) {
	$admin_toolbars = "$(\".cycle-toolbar\").css({ \"margin-left\" : \"464px\" });";
}

if(!empty($member_id)) {

$toolbarChange = "window.toolbarHeightChange =  '=197px';";
$toolbarMax = "window.toolbarMaxHeight = 197;";

$query =  ee()->db->query("SELECT `entry_id`, `entry_text`,`creation_date`, `last_updated`, `tag` FROM `otca_diary` `d` LEFT JOIN (`otca_user_status` `s`)  ON (`d`.`current_practice_cycle` = `s`.`history_id` AND `d`.`member_id` = `s`.`member_id`) WHERE `s`.`member_id` = '$member_id' AND `d`.`member_id` = '$member_id' LIMIT 0, 50");

$js_reflections_array = array();
if ($query->num_rows() > 0)
{
	$i = 0;
	foreach($query->result_array() as $row) // returns all diary entries for this practice cycle
    	{	
		//$entry_text_pregged = escapeJsonString($row['entry_text']);
		//$entry_text_pregged = json_encode($row['entry_text']);
		$entry_text_pregged = $row['entry_text'];
		$js_reflections_array[$row['tag']] = array("internalId" => $row['entry_id'], "text" => $entry_text_pregged, "date" => $row['creation_date'], "tag" => $row['tag'] );
	}
} 

$query = ee()->db->query("SELECT a.`steps`, a.`level`, a.`beginner`, a.`objectives`, a.`history_id`, a.`title`,b.`time` FROM `otca_user_status` a, `otca_user_status_history` b WHERE a.`member_id`=b.`member_id` AND a.`history_id`=b.`id` AND a.`member_id`= '$member_id' LIMIT 0,50");

$steps = "[]";
$level = '1';
$beginner = 'false';
$objectives = "[]";
$time = 0;
if ($query->num_rows() > 0)
{
	foreach($query->result_array() as $row) // returns one row
    {
 		$steps = isset($row['steps']) && $row['steps'] != ""?$row['steps']:"[]";
		$level = isset($row['level']) && $row['level'] != ""?$row['level']:1;
		$beginner = isset($row['beginner']) && $row['beginner'] != ""?$row['beginner']:0;
		$objectives = isset($row['objectives']) && $row['objectives'] != ""?$row['objectives']:"[]";
		$history_id = isset($row['history_id']) && $row['history_id'] != ""?$row['history_id']:0;
		$title = isset($row['title']) && $row['title'] != ""?$row['title']:"";
		$time = isset($row['time']) && $row['time'] != ""?$row['time']:0;
	}
}	else {
	$steps = '[]';
	$level = 1;
	$beginner = 1;
	$objectives = '[]';
	$history_id = 0;
	$title = "";
	$time = 0;
}
if($time == 0) 
	$js_humanTime = "window.cycleCreated = \"Not yet saved\";\n";
else
	$js_humanTime = "window.cycleCreated = \"" . ee()->localize->format_date('%d %M, %Y', $time) . "\";\n";

$screen_name = ee()->session->userdata("screen_name");		
$js_screen_name = "window.screen_name = \"$screen_name\";\n";
if (empty($js_reflections_array)) {
	$json_ref_array = "{}";
} else {
	$json_ref_array = json_encode($js_reflections_array);
}

$js_userProfile = "window.userProfile = {
				\"reflections\" : $json_ref_array,
				\"startNewCycle\" : false,
				\"steps\" : $steps,
				\"level\" : $level,
				\"beginner\" : $beginner,
                \"objectives\" : $objectives,
				\"history_id\" : $history_id,
				\"title\" : \"$title\",
				\"time\" : $time
			};\n\n";
			
$js_currentTime = "window.currentTime = \"".ee()->localize->format_date('%l %d%S %M, %Y %g:%i %a')."\";\n";
			
// prepare cycles for later display in groups on UI
$sql = "SELECT id, title, time FROM otca_user_status_history WHERE member_id='$this->member_id' ORDER BY time DESC LIMIT 0, 500";
$query =  ee()->db->query($sql);
$cycle_result = "var cycleNames = ".json_encode($query->result_array());
//echo $cycle_result;

$end_point = $this->MAX_ASSESSED_ITEMS; //$this->START_ASSESS + floor($this->MAX_ASSESSED_ITEMS / 2); 
// get item directly, as self assessed regardless of whether or not it has been assessed by an educator. 
$sql = "SELECT title.entry_date, data.entry_id, title.author_id, data.field_id_6 AS self_assessment,  data.field_id_13 as step, 
data.field_id_14 as level, data.field_id_3 as file_url, ev.filename, student.screen_name, student.email, student.group_id, title.title
FROM exp_channel_data data, exp_channel_titles title, otca_evidence ev, exp_members student WHERE data.entry_id = ev.entry_id
AND title.entry_id = ev.entry_id AND student.member_id = title.author_id AND student.member_id = '$this->member_id' 
ORDER BY title.entry_date DESC";// LIMIT $this->START_ASSESS, $end_point";

$query =  ee()->db->query($sql);
$numrows = $query->num_rows();
$self_assessed_array = array();
if ($query->num_rows() > 0)	
{
	foreach($query->result_array() as $row) // returns all self-assessed matrix statements for this evidence
	{
		$self_assessment = $row['self_assessment'];
		
		$file_link = strlen($row['file_url']) > 0? "$row[file_url]/$row[entry_id]":"";
		$file_link = preg_replace('~\{(.*?)\}~', '/download/secure/', $file_link);
		
		$self_assessed_array[] = array('is_current_entry' => false, 'entry_date' => ee()->localize->format_date('%D, %F %d, %Y',$row['entry_date']),
		                               'time' => $row['entry_date'], 'entry_id' => $row['entry_id'], 'title' => $row['title'], 'group_id' => $row['group_id'], 
		                               'self_assessment' => "$self_assessment", 'step' => $row['step'], 'level' => $row['level'], 'file_link' => $file_link);
	}
}

if($this->USE_VERIFICATION === TRUE) {
/* assessed sql section */
$sql = "SELECT * FROM (SELECT title.entry_date, data.entry_id, title.author_id, title.title, 
        data.field_id_6 as self_assessment, data.field_id_13 as step, data.field_id_14 as level, 
        av.matrix_ids as supervisor_assessment, av.feedback, av.date_assessed, m.screen_name, m.email, 
        m.group_id FROM exp_channel_data data, exp_channel_titles title , otca_evidence ev, otca_evidence_validated av, 
        exp_members m, exp_members student WHERE data.entry_id = ev.entry_id 
            AND title.entry_id = ev.entry_id 
            AND ev.entry_id = av.evidence_id 
            AND m.member_id = av.assessor_id 
            AND student.member_id = title.author_id 
            AND student.member_id ='$this->member_id' 
        ORDER BY av.date_assessed DESC, 
        m.group_id, data.entry_id) ua";
        //LIMIT $this->START_ASSESS, $end_point";

$debugsql = $sql;

$query =  ee()->db->query($sql);

if ($query->num_rows() > 0)
{
	$rightnow = ee()->localize->now;
	$mostrecent = 0;
	foreach($query->result_array() as $row) // returns all assessed matrix statements for this evidence
       {	
		$sa = $row['supervisor_assessment'];
		$selfa = $row['self_assessment'];
		$date_assessed = ee()->localize->format_date('%D, %F %d, %Y %g:%i:%s%a', $row['date_assessed']);
		//$current = $row['entry_id'] == $this->id;  no longer used since we now have one view of the entire matrix
		$current = FALSE;
		$assess_array[] = array('raw_date' => $row['date_assessed'], 'is_current_entry' => $current,
		                  'entry_date' => ee()->localize->format_date('%D, %F %d, %Y %g:%i:%s%a',$row['entry_date']), 
		                  'entry_id' => $row['entry_id'], 'title' => $row['title'] /* added 5/06/13 */, 
		                  'date_assessed' => $date_assessed, 'supervisor_assessment' => "$sa", 
		                  'screen_name' => $row['screen_name'], 'email' => $row['email'], 'group_id' => $row['group_id'], 
		                  'self_assessment' => "$selfa", 'feedback' => $row['feedback'], 
		                  'step' => $row['step'], 'level' => $row['level'], 'time' => $row['entry_date']);
	}
}
}

if(isset($self_assessed_array)) {
	$sa_item = json_encode($self_assessed_array);
	$self_assessed_item_js = "window.self_assessed_item = $sa_item;\n";
} else {
    $self_assessed_item_js = "window.self_assessed_item = [];\n";
}

if(isset($assess_array)) {
	$items = json_encode($assess_array);
	$assessed_items_js = "window.assessed_items = $items;\n\n";
} else {
    $assessed_items_js = "window.assessed_items = [];\n\n";
}

$truthy = $this->has_next_assess() ? "true;" : "false;";
$next_assess = "window.nextAssessment = $truthy";
$truthy = $this->has_prev_assess() ? "true;" : "false;";
$prev_assess = "window.prevAssessment = $truthy";
$max_assess = "window.maxAssessPerPage = $this->MAX_ASSESSED_ITEMS;";

$verify =  $this->USE_VERIFICATION === TRUE ? "true" : "false";
/* user javascript variables */	
$data = "";
$data .= "window.USE_VERIFICATION = $verify;\n";
$data .= $js_userProfile . "\n";
$data .= $js_humanTime . "\n";
$data .= $js_screen_name . "\n";
$data .= $js_currentTime . "\n";
$data .= $toolbarChange . "\n";
$data .= $toolbarMax . "\n";
$data .= $admin_toolbars . "\n";
$data .= $next_assess . "\n";
$data .= $prev_assess . "\n";
$data .= $max_assess . "\n";
//$data .= "console.log(".json_encode($debugsql).");";

if(!empty($self_assessed_item_js)) { 	
	$data .= $self_assessed_item_js;
}
if(!empty($assessed_items_js)) { 
	$data .= $assessed_items_js;
}
$data .= $cycle_result;

return $data;
}
}

public function get_top_js() {
	$member_id = ee()->session->userdata('member_id');
	$is_guest = empty($member_id);
	
	ob_start(); 
	include_once('main_top.js');
	$buffer = ob_get_contents();
	ob_end_clean();
	return $buffer;	
}


 
public static function usage()
{
        ob_start();  ?>

Saves OTCA user status for otca.net.au.

POST a JSON object to the save function to persist the current practice cycle. Use the restore function to retrieve the user's cycle.
The profile consists of user reflections and general settings for the practice cycle.  
Reflections (javascript: userProfile.reflections[]) are persisted in the otca_diary mySQL table.
The user status (javascript: userProfile.steps, userProfile.level, userProfile.beginner, userProfile.objectives) 
are persisted to the otca_user_status mySQL table.

    <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
}    
}
